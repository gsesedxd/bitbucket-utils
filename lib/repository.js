/**********************************************************************
* Project           : Bitbucket
* Program name      : repository.js
* Author            : AGM
* Date created      : 20190614
* Purpose           : Create or Update a repository of bitbucket
**********************************************************************/


var utils = require("./utils");
var inputsRequest = require("./input");

function createRepository(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            let result;
            //Check if we have the username & token
            if (!authenticate || !authenticate.username || !authenticate.token) {
                result.status = "FAILED"
                result.error = [{ "Enviroment": "Enviroment Variables not set", "Remember": "Config 'username' and 'token' variables for access bitbucket" }]
                resolve(result);
            }else if (!repo || !utils.checkInput(repo, inputsRequest.repo)) {
                //Check the Input of the JSON is correct
                result.status = "FAILED"
                result.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.repo }]
                resolve(result);
            }else{
                result = createRepo(authenticate,repo);
                resolve(result);
            }
        });
    });
}

async function createRepo(authenticate, repo) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase();
    let body = {
        "scm": "git",
        "name": repo.reponame.toLowerCase(),
        "is_private": repo.visibility === 'private',
        "description": repo.description,
        "language": repo.language,
        "project": { key: repo.project },
        "has_issues": repo.issues,
        "has_wiki": repo.wiki,
        "fork_policy": "no_public_forks"
    };
    utils.clearBody(body);

    let method = "PUT";
    const response = await utils.apiCall(body, url, method);
    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "REPO_CREATED"
        responseEnd.repo_url = response.body.links.html.href;
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    return responseEnd;
}


// --------------------------
// Repo - init call

function repository(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            createRepository(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

exports.repository = repository;