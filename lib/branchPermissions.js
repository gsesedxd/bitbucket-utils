/**********************************************************************
* Project           : Bitbucket
* Program name      : branchPermissions.js
* Author            : AGM
* Date created      : 20190614
* Purpose           : Create or Update the branchs permissions of a repository of bitbucket
**********************************************************************/

var utils = require("./utils");
var inputsRequest = require("./input");

async function getBranchsPerms(authenticate, repo) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/branch-restrictions';

    let method = "GET";
    const response = await utils.apiCall({}, url, method);

    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "BRANCH_PERM_INFO"
        responseEnd.body = response.body.values;
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    return responseEnd;
}

async function createBranchRestric(authenticate, repo, branchR, resolve) {

    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/branch-restrictions';

    let body = branchR;

    let method = "POST";
    const response = await utils.apiCall(body, url, method);

    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "REPO_PERM"
        responseEnd.body = response.body.values;
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    resolve(responseEnd);
}

async function deleteBranchRestric(authenticate, repo, id, resolve) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/branch-restrictions/' + id;

    let method = "DELETE";
    const response = await utils.apiCall({}, url, method);

    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "PERM_DELETED"
        responseEnd.body = response.body.values;
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    resolve(responseEnd);
}

function preparePermissions(name, perm, users) {
    let build = {
        "kind": perm,
        "branch_match_kind": "glob",
        "pattern": name
    };
    
    if (users && users.length != 0) {
        build["users"] = [];
        users.forEach(user => {
            build["users"].push({"username": user});
        });
    }

    return build;
}

async function createPermissons(authenticate, repo, repoPerm, resolve) {
    //If the master is created, we continue with the branchs list and create the next ones
    promise_array = [];

    for (let index = 0; index < repo.branchs.length; index++) {
        const branch = repo.branchs[index];

        let rulesBit = repoPerm.filter(item => item.pattern === branch.name);
        
        promise_array.push(
            new Promise((resolve, reject) => {
                delCreaPerm(authenticate, repo, branch, rulesBit, resolve);
            }));
    }

    results = await Promise.all(promise_array);
    let responseEnd = {};
    responseEnd.error = [];
    if (results.length == 0) {
        responseEnd.status = "PERMISSIONS_IDE";
        responseEnd.body = "NO_RULES_CREATED";
        resolve(responseEnd);
        return false;
    } else {
        //Save the errors
        results.forEach(element => {
            if (element.error) {
                responseEnd.error.push(element.error)
            }
            responseEnd.status = (responseEnd.status == "FAILED") ? "FAILED" : element.status;
        });       
        resolve(responseEnd);
        return false;
    }
}

async function delCreaPerm(authenticate, repo, branch, rulesBit, resolve) {

    //Delete Perms
    let delete_promise_array = [];
    let results = [];
    if (rulesBit.length != 0) {
        for (let index = 0; index < rulesBit.length; index++) {
            const id = rulesBit[index].id;
            delete_promise_array.push(
                //Delete all the rules
                new Promise((resolve, reject) => {
                    deleteBranchRestric(authenticate, repo, id, resolve);
                })
            )
        }
        results = await Promise.all(delete_promise_array);
    }
    let responseEnd = {};
    if (results.length == 0) {
        responseEnd.status = "PERMISSIONS_IDE";
        responseEnd.body = "No_Deletes";
        if (!branch.permissions || branch.permissions.length == 0) {
            resolve(responseEnd);
            return false;
        }
    } else {
        //Save the errors
        results.forEach(element => {
            if (element.error) {
                responseEnd.error.push(element.error)
            }
            responseEnd.status = (responseEnd.status == "FAILED") ? "FAILED" : element.status;
        });
        if (responseEnd.status == "FAILED") {
            resolve(responseEnd);
            return false;
        }
    }

    let promise_array = [];
    for (let index = 0; index < branch.permissions.length; index++) {
        const permission = branch.permissions[index];
        //Check permissions
        if (!permission || !utils.checkInput(permission, inputsRequest.permissions)) {
            responseEnd.status = "FAILED"
            responseEnd.body = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.permissions }]
            resolve(responseEnd);
            return false;
        } else {
            let brBuild = preparePermissions(branch.name, permission.perm, permission.usernames);
            promise_array.push(
                //Delete all the rules
                new Promise((resolve, reject) => {
                    createBranchRestric(authenticate, repo, brBuild, resolve);
                })
            )
        }
    }
    results = await Promise.all(promise_array);
    responseEnd = {};
    responseEnd.error = [];
    if (results.length == 0) {
        responseEnd.status = "PERMISSIONS_IDE";
        responseEnd.body = "NO_RULES_CREATED";
        resolve(responseEnd);
        return false;
    } else {
        //Save the errors
        results.forEach(element => {
            if (element.error) {
                responseEnd.error.push(element.error)
            }
            responseEnd.status = (responseEnd.status == "FAILED") ? "FAILED" : element.status;
        });
        resolve(responseEnd);
        return false;
    }
}

function checkPermissions(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            let result;
            checkRepo(authenticate, repo).then(result => {
                if (result.status != "FAILED") {
                    if (!repo.branchs || !Array.isArray(repo.branchs)) {
                        //Check the Input of the JSON is correct
                        result.status = "FAILED"
                        result.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.branch }]
                        resolve(result);
                    } else {
                        waitPerm(authenticate, repo, result.body).then(result => {
                            resolve(result);
                        });
                    }
                } else {
                    resolve(result);
                }
            });
        });
    });
}

function waitPerm(authenticate, repo, repoPerm) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            createPermissons(authenticate, repo, repoPerm, resolve).then(result => {
                resolve(result);
            });
        });
    });
}


function checkRepo(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            let result = {};
            //Check if we have the username & token
            if (!authenticate || !authenticate.username || !authenticate.token) {
                result.status = "FAILED"
                result.error = [{ "Enviroment": "Enviroment Variables not set", "Remember": "Confi 'username' and 'token' variables for access bitbucket" }]
                resolve(result);
            }else if (!repo || !utils.checkInput(repo, inputsRequest.repo)) {
                //Check the Input of the JSON is correct
                result.status = "FAILED"
                result.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.repo }]
                resolve(result);
            }else{
                result = getBranchsPerms(authenticate, repo);
                resolve(result);
            }
        });
    });
}
// --------------------------
// Repo - init call

function branchPermissions(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkPermissions(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}


function retriveRepoPerm(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkRepo(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

exports.retriveRepoPerm = retriveRepoPerm;
exports.branchPermissions = branchPermissions;