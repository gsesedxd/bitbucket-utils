/** ********************************************************************
* Project           : Bitbucket
* Program name      : commit.js
* Author            : GSS
* Date created      : 20190920
* Purpose           : Sets a state on the commit build
********************************************************************* */

const utils = require('./utils');
const inputs = require('./input');

const checkCommitBuild = ({ repo, commit }, commitStatus) => new Promise((resolve) => {
  // Comprobar el input
  process.nextTick(() => {
    let result = {};
    // Check if we have the username & token
    if (!repo || !utils.checkInput(repo, inputs.repo)) {
      // Check the Input of the JSON is correct
      result.status = 'FAILED';
      result.error = [{ 'Input Error': 'Please check the inputKeys value to solve the problem', inputKeys: inputs.repo }];
      resolve(result);
    } else if (!commit || !utils.checkInput(commit, inputs.commit)) {
      // Check the Input of the JSON is correct
      result.status = 'FAILED';
      result.error = [{ 'Input Error': 'Please check the inputKeys value to solve the problem', inputKeys: inputs.commit }];
      resolve(result);
    } else {
      const status = checkCommitStatus(commitStatus);
      if (!status) {
        // Check the Input of the JSON is correct
        result.status = 'FAILED';
        result.error = [{ 'Input Error': 'Please check the inputKeys value to solve the problem', commitStatus }];
        resolve(result);
      } else {
        result = setCommitBuild({ repo, commit }, status);
        resolve(result);
      }
    }
  });
});

const setCommitBuild = async ({ repo, commit }, commitStatus) => {
  // const API_URL = `https://${authenticate.username}:${authenticate.token}@api.bitbucket.org/2.0/repositories/${repo.usernameRepo}/${repo.reponame.toLowerCase()}/commit/${repo.commitNode}/statuses/build;
  // No authentication
  const API_URL = `https://api.bitbucket.org/2.0/repositories/${repo.usernameRepo}/${repo.reponame.toLowerCase()}/commit/${commit.commitNode}/statuses/build`;
  // WIP
  const METHOD = 'POST';
  const BODY = {
    key: 'build',
    url: 'https://foo.com/builds/foo/bar',
    state: commitStatus,
    /*
        SUCCESSFUL
        FAILED
        INPROGRESS
        STOPPED
        */
  };

  const response = await utils.apiCall(BODY, API_URL, METHOD);
  const responseEnd = {};
  if (response.statusCode >= 200 && response.statusCode < 300) {
    responseEnd.status = 'COMMIT_BUILD_SET';
    responseEnd.body = response.body.values;
  } else {
    responseEnd.status = 'FAILED';
    responseEnd.error = response;
  }
  return responseEnd;
};

const checkCommitStatus = (commitStatus) => {
  const STATUSES = ['SUCCESSFUL', 'FAILED', 'INPROGRESS', 'STOPPED'];
  return STATUSES.find((status) => status === commitStatus);
};

// init call
const commitBuild = ({ repo, commit }, commitStatus, callback) => new Promise((resolve) => {
  process.nextTick(() => {
    checkCommitBuild({ repo, commit }, commitStatus).then((result) => {
      if (callback) { callback(result); }
      resolve(result);
    });
  });
});


exports.commitBuild = commitBuild;
