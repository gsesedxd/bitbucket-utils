/**********************************************************************
* Project           : Bitbucket
* Program name      : branch.js
* Author            : AGM
* Date created      : 20190614
* Purpose           : Create the branchs of a repository of bitbucket, if there are no branchs, the first branch have to be the 'master'
**********************************************************************/


var utils = require("./utils");
var inputsRequest = require("./input");

//POST

async function createBranchPOST(authenticate, repo, branch, resolve) {
    let url;
    let response;
    //The endpoint is different if the master is not created
    if (branch.name == "master") {
        url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/src?branch=' + branch.name;
        utils.createFile(branch.file.filename, branch.file.message);
        let method = "POST";
        response = await utils.apiCallFile(url, method, branch.file.filename);
    } else {
        url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/refs/branches';
        let method = "POST";

        let body = {
            "name": branch.name,
            "target": {
                "hash": branch.fromBranch,
            }
        }
        response = await utils.apiCall(body, url, method);
    }

    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "BRANCH_CREATED"
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    resolve(responseEnd);
}

async function postBranch(authenticate, repo, branchs, resolve) {
    //Get the needed information of the branches that are configured in bitbucket server
    var descriptionBranchs = branchs.map(i => { return { name: i.name } });

    let promise_array = [];
    let results;
    let responseEnd = {};
    responseEnd.error = [];
    //First, if the master branch is not created, we will created
    if (branchs.length == 0) {
        var desc = repo.branchs.find(function(e) {
            return e.name == "master"
        })

        if(desc){
            if (!desc.file || !utils.checkInput(desc.file, inputsRequest.file)) {
                responseEnd.status = "FAILED"
                responseEnd.body = [{ "Input Error": "Please check the inputKeys value to solve the problem of branch 'master'", "inputKeys": inputsRequest.file }]
                resolve(responseEnd);
                return false;
            }
            promise_array.push(
                new Promise((resolve, reject) => {
                    createBranchPOST(authenticate, repo, desc, resolve);
                })
            )
            results = await Promise.all(promise_array);
        }else{
            responseEnd.status = "FAILED"
            responseEnd.body = [{ "Input Error": "'master' branch needed when there are no other branchs in the repository"}]
            resolve(responseEnd);
            return false;
        }
    }
    if (results && results[0].status == "FAILED") {
        responseEnd.status = "BRANCH_IDE";
        responseEnd.body = "Check if the branchs already exist";
        resolve(event);
        return false;
    } else {
        
        //If the master is created, we continue with the branchs list and create the next ones
        promise_array = [];
        for (let index = 0; index < repo.branchs.length; index++) {
            const branch = repo.branchs[index];

            if (branch.name != "master") {
                //Check the Input of the JSON is correct
                if (!branch || !utils.checkInput(branch, inputsRequest.branch)) {
                    responseEnd.status = "FAILED"
                    responseEnd.body = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.branch }]
                    resolve(responseEnd);
                    return false;
                }
                var dsc = descriptionBranchs.filter(item => item.name === branch.name);
                if (dsc.length == 0) {
                    //Create branch
                    promise_array.push(
                        new Promise((resolve, reject) => {
                            createBranchPOST(authenticate, repo, branch, resolve);
                        })
                    )
                }
            }
        }
    }
    //Wait all the promise to end
    results = await Promise.all(promise_array);

    if (results.length == 0) {
        responseEnd.status = "BRANCH_IDE";
        responseEnd.body = "Check if the keys are the same";
    } else {
        //Save the errors
        results.forEach(element => {
            if (element.error) {
                responseEnd.error.push(element.error)
            }
            responseEnd.status = (responseEnd.status == "FAILED") ? "FAILED" : element.status;
        });
    }
    resolve(responseEnd);
}

function waitBranch(authenticate, repo, branchs) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            postBranch(authenticate, repo, branchs, resolve).then(result => {
                resolve(result);
            });
        });
    });
}

function checkPOSTBranch(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkGETBranch(authenticate, repo).then(result => {
                if (result.status != "FAILED") {
                    waitBranch(authenticate, repo, result.body).then(result => {
                        resolve(result);
                    });
                } else {
                    resolve(result);
                }
            });
        });
    });
}

//GET

function checkGETBranch(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            let result = {};
            //Check if we have the username & token
            if (!authenticate || !authenticate.username || !authenticate.token) {
                result.status = "FAILED"
                result.error = [{ "Enviroment": "Enviroment Variables not set", "Remember": "Confi 'username' and 'token' variables for access bitbucket" }]
                resolve(result);
            }else if (!repo || !utils.checkInput(repo, inputsRequest.repo)) {
                //Check the Input of the JSON is correct
                result.status = "FAILED"
                result.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.repo }]
                resolve(result);
            }else{
                result = getBranchs(authenticate, repo);
                resolve(result);
            }
        });
    });
}

async function getBranchs(authenticate, repo) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/refs/branches';

    let method = "GET";
    const response = await utils.apiCall({}, url, method);

    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "BRANCH_INFO"
        responseEnd.body = response.body.values;
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    return responseEnd;
}


// --------------------------
// Repo - init call

function createBranch(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkPOSTBranch(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

function retrieveBranch(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkGETBranch(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

exports.createBranch = createBranch;
exports.retrieveBranch = retrieveBranch;