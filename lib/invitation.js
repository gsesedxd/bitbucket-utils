/**********************************************************************
* Project           : Bitbucket
* Program name      : invitation.js
* Author            : AGM
* Date created      : 20190614
* Purpose           : Invite users to the respository with their emails
**********************************************************************/


var utils = require("./utils");
var inputsRequest = require("./input");

function checkInvitation(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            let result;
            //Check if we have the username & token
            if (!authenticate || !authenticate.username || !authenticate.token) {
                result.status = "FAILED"
                result.error = [{ "Enviroment": "Enviroment Variables not set", "Remember": "Config 'username' and 'token' variables for access bitbucket" }]
                resolve(result);
            } else if (!repo || !utils.checkInput(repo, inputsRequest.repo)) {
                //Check the Input of the JSON is correct
                result.status = "FAILED"
                result.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.repo }]
                resolve(result);
            } else if (!repo.invitation || !Array.isArray(repo.invitation)) {
                //Check the Input of the JSON is correct
                result.status = "FAILED"
                result.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.repo }]
                resolve(result);
            } else {
                sendInvitation(authenticate, repo, resolve)
            }
        });
    });
}


async function sendInvitation(authenticate, repo, resolve) {
    let promise_array = [];

    for (let index = 0; index < repo.invitation.length; index++) {
        const user = repo.invitation[index];
        if (utils.checkInput(user, inputsRequest.invitation) && utils.validateEmail(user.email)) {
            promise_array.push(
                new Promise((resolve, reject) => {
                    createInvite(authenticate, repo, user, resolve);
                })
            )
        }
    }

    let results = await Promise.all(promise_array);

    let responseEnd = {};
    responseEnd.error = [];
    if (results.length == 0) {
        responseEnd.status = "SSH_IDE";
        responseEnd.body = "Check the emails";
    } else {
        //Save the errors
        results.forEach(element => {
            if (element.error) {
                responseEnd.error.push(element.error)
            }
            responseEnd.status = (responseEnd.status == "FAILED") ? "FAILED" : element.status;
        });
    }
    resolve(responseEnd);
}

async function createInvite(authenticate, repo, user, resolve) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/1.0/invitations/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/';

    let body = {
        "email": user.email,
        "permission": user.perm
    };

    let method = "POST";
    const response = await utils.apiCall(body, url, method);

    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "INVITATIONS_SEND"
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    resolve(responseEnd);
}


// --------------------------
// Repo - init call

function invitation(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkInvitation(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

exports.invitation = invitation;