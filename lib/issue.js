/** ********************************************************************
* Project           : Bitbucket
* Program name      : issue.js
* Author            : GSS
* Date created      : 20190923
* Purpose           : Manage repository issues
********************************************************************* */

const utils = require('./utils');
const inputs = require('./input');

const checkIssue = (authenticate, repo) => new Promise((resolve) => {
  // Comprobar el input
  const { issue } = repo;
  process.nextTick(() => {
    let result = {};
    // Check if we have the username & token
    if (!authenticate || !authenticate.username || !authenticate.token) {
      result.status = 'FAILED';
      result.error = [{ Enviroment: 'Enviroment Variables not set', Remember: "Configure 'username' and 'token' variables for access bitbucket" }];
      resolve(result);
    } else if (!repo || !utils.checkInput(repo, inputs.repo)) {
      // Check the Input of the JSON is correct
      result.status = 'FAILED';
      result.error = [{ 'Input Error': 'Please check the repo inputKeys value to solve the problem', inputKeys: inputs.repo }];
      resolve(result);
    } else if (!issue || !utils.checkInput(issue, inputs.issue)) {
      // Check the Input of the JSON is correct
      result.status = 'FAILED';
      result.error = [{ 'Input Error': 'Please check the issue inputKeys value to solve the problem', inputKeys: inputs.issue }];
      resolve(result);
    } else {
      result = createIssue(authenticate, repo, issue);
      resolve(result);
    }
  });
});

const createIssue = async (authenticate, repo, issue) => {
  const API_URL = `https://${authenticate.username}:${authenticate.token}@api.bitbucket.org/2.0/repositories/${repo.usernameRepo}/${repo.reponame.toLowerCase()}/issues`;
  const METHOD = 'POST';
  // TODO: Ver si es repo.issue o issue en si lo que se pasa por parametro

  const BODY = {
    title: issue.title,
    content: {
      raw: issue.content,
      markup: issue.textFormat,
    },
    kind: issue.type,
    priority: issue.priority,
    state: issue.state,
  };
    // WIP
  let response;
  const responseEnd = {};
  try {
    response = await utils.apiCall(BODY, API_URL, METHOD);
  } catch (error) {
    responseEnd.status = 'FAILED';
    responseEnd.error = error;
    return responseEnd;
  }
  if (response.statusCode >= 200 && response.statusCode < 300) {
    responseEnd.status = 'ISSUE_CREATED';
    responseEnd.body = response.body.values;
  } else {
    responseEnd.status = 'FAILED';
    responseEnd.error = response;
  }
  return responseEnd;
};


// init call
const addIssue = (authenticate, repo, callback) => new Promise((resolve) => {
  process.nextTick(() => {
    checkIssue(authenticate, repo).then((result) => {
      if (callback) { callback(result); }
      resolve(result);
    });
  });
});

exports.addIssue = addIssue;
