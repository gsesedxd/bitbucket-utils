
// ----------------------------------------------------------------------------------
// Dependencies
// ----------------------------------------------------------------------------------

const repository = require('./repository');
const webhook = require('./webhook');
const ssh = require('./ssh');
const branch = require('./branch');
const invitation = require('./invitation');
const branchPermissions = require('./branchPermissions');
const retriveUserDataFromEmail = require('./getUserDataFromEmail');
const repoDownloads = require('./repoDownloads');
const issue = require('./issue');
const alterCommit = require('./commit');

exports.retriveUserDataFromEmail = retriveUserDataFromEmail.retriveUserDataFromEmail;
exports.retriveRepoPerm = branchPermissions.retriveRepoPerm;
exports.branchPermissions = branchPermissions.branchPermissions;
exports.invitation = invitation.invitation;
exports.branch = branch.createBranch;
exports.retrieveBranch = branch.retrieveBranch;
exports.ssh = ssh.SSH;
exports.retriveSSH = ssh.retriveSSH;
exports.webhook = webhook.webhooks;
exports.retriveWebhooks = webhook.retriveWebhooks;
exports.repository = repository.repository;
exports.issue = issue.addIssue;
exports.downloadsManager = repoDownloads.downloadsManager;
exports.commitBuild = alterCommit.commitBuild;
