/**********************************************************************
* Project           : Bitbucket
* Program name      : webhook.js
* Author            : AGM
* Date created      : 20190614
* Purpose           : Create Webhooks
**********************************************************************/


var utils = require("./utils");
var inputsRequest = require("./input");



//POST


async function updateHook(authenticate, repo, uuid, webhook, resolve) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/hooks/' + uuid;

    let body = {
        "description": webhook.description,
        "url": webhook.url,
        "active": true,
        "events": webhook.permits
    };
    utils.clearBody(body);

    let method = "PUT";
    const response = await utils.apiCall(body, url, method);
    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "WEBHOOK_UPDATED"
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    resolve(responseEnd);
}

async function createHook(authenticate, repo, webhook, resolve) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/hooks';

    let body = {
        "description": webhook.description,
        "url": webhook.url,
        "active": true,
        "events": webhook.permits
    };
    utils.clearBody(body);

    let method = "POST";
    const response = await utils.apiCall(body, url, method);
    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "WEBHOOK_CREATED"
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    resolve(responseEnd);
}

async function postWebhook(authenticate, repo, webhooks, resolve) {
    let response;
    //Get the needed information of the Webhooks that are configured in bitbucket server
    var descriptionWebhooks = webhooks.map(i => { return { uuid: i.uuid, description: i.description, url: i.url } });

    let promise_array = [];
    for (let index = 0; index < repo.webhooks.length; index++) {
        const webhook = repo.webhooks[index];

        //Check the Input of the JSON is correct
        if (!webhook || !utils.checkInput(webhook, inputsRequest.webhook)) {
            response.status = "FAILED"
            evenresponset.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.createWebhook }]
            resolve(responseEnd);
            return false;
        }
        var dsc = descriptionWebhooks.filter(item => item.description === webhook.description);

        if (dsc.length != 0) {
            //Update Webhook
            if (webhook.url != dsc[0].url) {
                promise_array.push(
                    new Promise((resolve, reject) => {
                        updateHook(authenticate, repo, dsc[0].uuid, webhook, resolve)
                    })
                )
            }
        } else {
            //Create Webhook
            promise_array.push(
                new Promise((resolve, reject) => {
                    createHook(authenticate, repo, webhook, resolve)
                })
            )
        }
    }
    //Wait all the promise to end

    let results = await Promise.all(promise_array);

    let responseEnd = {};
    responseEnd.error = [];
    if (results.length == 0) {
        responseEnd.status = "WEBHOOK_IDE";
        responseEnd.body = "Check if the urls are the same";
    } else {
        //Save the errors
        results.forEach(element => {
            if (element.error) {
                responseEnd.error.push(element.error)
            }
            responseEnd.status = (responseEnd.status == "FAILED") ? "FAILED" : element.status;
        });
    }
    resolve(responseEnd);
}

function waitWebhook(authenticate, repo, webhooks) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            postWebhook(authenticate, repo, webhooks, resolve).then(result => {
                resolve(result);
            });
        });
    });
}

function checkPOSTWebhook(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkGETWebhook(authenticate, repo).then(result => {
                if (result.status != "FAILED") {
                    waitWebhook(authenticate, repo, result.body).then(result => {
                        resolve(result);
                    });
                } else {
                    resolve(result);
                }
            });
        });
    });
}

//GET

function checkGETWebhook(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            let result = {};
            //Check if we have the username & token
            if (!authenticate || !authenticate.username || !authenticate.token) {
                result.status = "FAILED"
                result.error = [{ "Enviroment": "Enviroment Variables not set", "Remember": "Confi 'username' and 'token' variables for access bitbucket" }]
                resolve(result);
            }else if (!repo || !utils.checkInput(repo, inputsRequest.repo)) {
                //Check the Input of the JSON is correct
                result.status = "FAILED"
                result.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.repo }]
                resolve(result);
            }else{
                result = getWebhooks(authenticate, repo);
                resolve(result);
            }
        });
    });
}

async function getWebhooks(authenticate, repo) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/hooks';

    let method = "GET";
    const response = await utils.apiCall({}, url, method);

    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "WEBHOOK_INFO"
        responseEnd.body = response.body.values;
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    return responseEnd;
}


// --------------------------
// Repo - init call

function webhooks(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkPOSTWebhook(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

function retriveWebhooks(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkGETWebhook(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

exports.webhooks = webhooks;
exports.retriveWebhooks = retriveWebhooks;