module.exports = {
  repo: [
    { key: 'reponame', description: 'Repository Name (String)', needed: true },
    { key: 'visibility', description: 'public | private (String)', needed: false, default: 'private' },
    { key: 'description', description: 'Repository Description (String)', needed: false },
    { key: 'language', description: 'Programming Language, example: python (String)', needed: false },
    { key: 'wiki', description: 'true|false (Boolean)', needed: false, default: false },
    { key: 'issues', description: 'true|false(Boolean)', needed: false, default: false },
    { key: 'usernameRepo', description: 'Username Repository (String)', needed: true },
    { key: 'project', description: 'Key Project associate with the repository (String)', needed: false },
    { key: 'GeneralInfo', description: 'This data have to be inside de key "repoCustom" that is a JSON' },
  ],
  webhook: [
    { key: 'url', description: 'Repository Name (String)', needed: true },
    { key: 'description', description: 'Description of the Webhook (String)', needed: true },
    { key: 'permits', description: 'Permisions of the webhook: example : ["repo:push","issue:created","issue:updated"] (Array)', needed: true },
    { key: 'GeneralInfo', description: 'This data have to be inside de key "webhooks" that is an Array' },
  ],
  ssh: [
    { key: 'key', description: 'SSH key', needed: true },
    { key: 'label', description: 'Description of the SSH (String)', needed: true },
    { key: 'GeneralInfo', description: 'This data have to be inside de key "sshKeys" that is an Array' },
  ],
  branch: [
    { key: 'name', description: 'Name of the branch', needed: true },
    { key: 'fromBranch', description: 'Target repository to make the new branch', needed: false, default: 'master' },
    { key: 'file', description: "Only needed when preparing the 'master' first branch", needed: false },
    { key: 'permissions', description: 'Only needed when calling the brancPermissioms function.', needed: false },
    { key: 'GeneralInfo', description: 'This data have to be inside de key "branch" that is an Array' },
  ],
  file: [
    { key: 'filename', description: 'File name to be created', needed: true },
    { key: 'message', description: 'The input message', needed: true },
    { key: 'GeneralInfo', description: 'This data have to be inside de key "file"' },
  ],
  invitation: [
    { key: 'perm', description: 'The user permission, read, write or admin', needed: true },
    { key: 'email', description: 'Email of the user', needed: true },
    { key: 'GeneralInfo', description: 'This data have to be inside de key "invitation" that is an Array' },
  ],
  permissions: [
    { key: 'perm', description: 'Permision to define like delete, force...', needed: true },
    { key: 'usernames', description: 'Array of usernames', needed: false },
    { key: 'GeneralInfo', description: 'This data have to be inside de key "invitation" that is an Array' },
  ],
  issue: [
    { key: 'title', description: 'Title of the Issue (String)', needed: true },
    { key: 'content', description: 'Content (String)', needed: false, default: 'DxD-tests' },
    { key: 'kind', description: 'Kind: bug | enhancement | proposal | task (String)', needed: true },
    { key: 'priority', description: 'Priority: trivial | minor | major | critical | blocker (String)', needed: true },
    { key: 'state', description: 'State: new | open | resolved | on hold | invalid | duplicate | wontfix | closed (String)', needed: false, default: 'new' },
    { key: 'GeneralInfo', description: 'This data have to be inside de key "issue" that is a JSON' },
  ],
  commit: [
    { key: 'node', description: 'Node (String)', needed: true },
    { key: 'status', description: 'Status (String)', needed: true },
    { key: 'GeneralInfo', description: 'This data have to be inside de key "issue" that is a JSON' },
  ],
  downloads: [
    { key: 'filePath', description: 'Path (String)', needed: true },
  ],
};
