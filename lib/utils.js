/** ********************************************************************
* Project           : Bitbucket
* Program name      : utils.js
* Author            : AGM
* Date created      : 20190614
* Purpose           : Utils
********************************************************************* */


const request = require('request');
const fs = require('fs');
const path = require('path');

/**
 * Checks if the provided input follows the specified rules
 * @param {*} event values of the input
 * @param {*} inputChecker reference inputs to be checked with event
 * @returns boolean
 */
function checkInput(event, inputChecker) {
  if (!event) {
    return false;
  }
  const defaults = inputChecker.filter((x) => x.default);
  defaults.forEach((element) => {
    if (!event[element.key]) {
      event[element.key] = element.default;
    }
  });

  return inputChecker.every((data) => {
    if (data.needed === true) {
      return Object.keys(event).includes(data.key);
    }
    return true;
  });
}

// Clear a json empty keys
function clearBody(body) {
  Object.keys(body).forEach((key) => body[key] === undefined && delete body[key]);
}


/**
 * Makes a HTTP Call to the URL with the method expecified.
 * @param {*} body body of the request
 * @param {*} url url of the request
 * @param {*} method method of the request
 * @returns Promise
 */
async function apiCall(body, url, method) {
  return new Promise(((resolve, reject) => {
    request({
      url,
      method,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    }, (err, res) => {
      if (err) {
        reject(err);
      }
      let body = {};
      if (res.body) {
        try {
          body = JSON.parse(res.body);
        } catch (error) {
          console.log(res.body);
          body = res.body;
        }
      }
      const response = {};
      switch (res.statusCode) {
      case 401:
      case 400:
        response.statusCode = res.statusCode;
        if (!body) {
          response.message = { 'Error:': 'input invalid or authentication error' };
        } else {
          response.message = body;
        }
        resolve(response);
        break;
      case 200:
      case 201:
      case 204:
        response.statusCode = res.statusCode;
        response.body = body;
        resolve(response);
        break;
      default:
        console.log(`${res.statusCode} Option not defined`);
        resolve({ statusCode: res.statusCode, body: res.body });
        break;
      }
    });
  }));
}

/**
 * Makes a HTTP Call to the URL with the method expecified uploading a file.
 * @param {*} url url of the request
 * @param {*} method method of the request
 * @param {*} filename path to the file to be uploaded
 * @returns Promise
 */
async function apiCallFile(url, method, filename) {
  return new Promise(((resolve, reject) => {
    request({
      url,
      method,
      headers: { 'Content-Type': 'multipart/form-data' },
      formData: {
        [filename]: fs.createReadStream(filename),
      },
    }, (err, res) => {
      if (err) {
        reject(err);
      }
      let body = {};
      if (res.body) {
        body = JSON.parse(res.body);
      }
      const response = {};
      switch (res.statusCode) {
      case 401:
      case 400:
        response.statusCode = res.statusCode;
        if (!body) {
          response.message = { 'Error:': 'input invalid or authentication error' };
        } else {
          response.message = body;
        }
        resolve(response);
        break;
      case 200:
      case 201:
      case 204:
        response.statusCode = res.statusCode;
        response.body = body;
        resolve(response);
        break;
      default:
        console.log('Option not defined');
        resolve(res);
        break;
      }
    });
  }));
}

// Create the README file for the master
function createFile(filename, message) {
  // change the cwd to /tmp
  process.chdir('/tmp');
  // make your modifications to the local repository
  fs.appendFileSync(
    path.join(process.cwd(), filename),
    message,
  );
}

/**
 * Validates email with RegEx
 * @param {*} email Email to validate
 * @returns boolean
 */
function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}


exports.validateEmail = validateEmail;
exports.createFile = createFile;
exports.clearBody = clearBody;
exports.checkInput = checkInput;
exports.apiCallFile = apiCallFile;
exports.apiCall = apiCall;
