/** ********************************************************************
* Project           : Bitbucket
* Program name      : repoDownloads.js
* Author            : GSS
* Date created      : 20190920
* Purpose           : Manage the uploading or getting files on the downloads repository section
********************************************************************* */

const utils = require('./utils');
const input = require('./input');

const checkUpdateDownloads = ({ repo, file }) => new Promise((resolve) => {
  // Comprobar el input
  process.nextTick(() => {
    let result = {};
    // Check if we have the username & token
    if (!repo || !utils.checkInput(repo, input.repo)) {
      // Check the Input of the JSON is correct
      result.status = 'FAILED';
      result.error = [{ 'Input Error': 'Please check the inputKeys value to solve the problem', inputKeys: input.repo }];
      resolve(result);
    } else if (!file || !utils.checkInput(file, input.downloads)) {
      // Check the Input of the JSON is correct
      result.status = 'FAILED';
      result.error = [{ 'Input Error': 'Please check the inputKeys value to solve the problem', inputKeys: input.downloads }];
      resolve(result);
    } else {
      result = updateDownloads({ repo, file });
      resolve(result);
    }
  });
});

const updateDownloads = async ({ repo, file }) => {
  // const API_URL = `https://${authenticate.username}:${authenticate.token}@api.bitbucket.org/2.0/repositories/${repo.usernameRepo}/${repo.reponame.toLowerCase()}/downloads`;

  // No authentication
  const API_URL = `https://api.bitbucket.org/2.0/repositories/${repo.usernameRepo}/${repo.reponame.toLowerCase()}/downloads`;
  const METHOD = 'POST';
  // WIP
  const response = await utils.apiCallFile(API_URL, METHOD, file.fileName);
  const responseEnd = {};
  if (response.statusCode >= 200 && response.statusCode < 300) {
    responseEnd.status = 'UPLOADED_FILE';
    responseEnd.body = response.body.values;
  } else {
    responseEnd.status = 'FAILED';
    responseEnd.error = response;
  }
  return responseEnd;
};

const checkDownloads = ({ repo, file }) => new Promise((resolve) => {
  // Comprobar el input
  process.nextTick(() => {
    let result = {};
    if (!repo || !utils.checkInput(repo, input.repo)) {
      // Check the Input of the JSON is correct
      result.status = 'FAILED';
      result.error = [{ 'Input Error': 'Please check the inputKeys value to solve the problem', inputKeys: input.repo }];
      resolve(result);
    } else if (!file || !utils.checkInput(file, input.downloads)) {
      // Check the Input of the JSON is correct
      result.status = 'FAILED';
      result.error = [{ 'Input Error': 'Please check the inputKeys value to solve the problem', inputKeys: input.downloads }];
      resolve(result);
    } else {
      result = getDownloads({ repo, file });
      resolve(result);
    }
  });
});

const getDownloads = async ({ repo, file }) => {
  // const API_URL = `https://${authenticate.username}:${authenticate.token}@api.bitbucket.org/2.0/repositories/${repo.usernameRepo}/${repo.reponame.toLowerCase()}/downloads/${repo.fileName}`;
  // No authentication
  const API_URL = `https://api.bitbucket.org/2.0/repositories/${repo.usernameRepo}/${repo.reponame.toLowerCase()}/downloads/${file.fileName}`;
  const METHOD = 'GET';
  const response = await utils.apiCall({}, API_URL, METHOD);
  const responseEnd = {};
  if (response.statusCode >= 200 && response.statusCode < 300) {
    responseEnd.status = 'DOWNLOADED_FILE';
    responseEnd.body = response.body.values;
  } else {
    responseEnd.status = 'FAILED';
    responseEnd.error = response;
  }
  return responseEnd;
};


// init call
const getDownloadFile = ({ repo, file }, callback) => new Promise((resolve) => {
  process.nextTick(() => {
    checkDownloads({ repo, file }).then((result) => {
      if (callback) { callback(result); }
      resolve(result);
    });
  });
});

const setDownloadFile = ({ repo, file }, callback) => new Promise((resolve) => {
  process.nextTick(() => {
    checkUpdateDownloads({ repo, file }).then((result) => {
      if (callback) { callback(result); }
      resolve(result);
    });
  });
});

exports.downloadsManager = {
  getDownloadFile,
  setDownloadFile,
};
