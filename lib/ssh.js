/**********************************************************************
* Project           : Bitbucket
* Program name      : ssh.js
* Author            : AGM
* Date created      : 20190614
* Purpose           : Create or Update the ssh access keys from a repository of bitbucket
**********************************************************************/

var utils = require("./utils");
var inputsRequest = require("./input");

//POST

async function updateSSH(authenticate, repo, key_id, sshkey, resolve) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/deploy-keys/' + key_id;

    let method = "DELETE";

    const response = await utils.apiCall({}, url, method);
    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        createSSH(authenticate, repo, sshkey, resolve);
    } else {
        responseEnd.status = "FAILED";
        responseEnd.error = response;
        resolve(responseEnd);
    }
}

async function createSSH(authenticate, repo, sshkey, resolve) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/deploy-keys';

    let body = {
        "key": sshkey.key,
        "label": sshkey.label
    };

    let method = "POST";
    const response = await utils.apiCall(body, url, method);
    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "SSH_CREATED"
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    resolve(responseEnd);
}

async function postSSH(authenticate, repo, sshkeys, resolve) {
    let response;
    //Get the needed information of the SSH that are configured in bitbucket server
    var descriptionSSHKeys = sshkeys.map(i => { return { key: i.key, id: i.id, label: i.label } });

    let promise_array = [];
    for (let index = 0; index < repo.sshKeys.length; index++) {
        const sshkey = repo.sshKeys[index];

        //Check the Input of the JSON is correct
        if (!sshkey || !utils.checkInput(sshkey, inputsRequest.ssh)) {
            response.status = "FAILED"
            evenresponset.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.ssh }]
            resolve(responseEnd);
            return false;
        }
        var dsc = descriptionSSHKeys.filter(item => item.label === sshkey.label);
 
        if (dsc.length != 0) {
            //Update SSH
            if (sshkey.key != dsc[0].key) {
                promise_array.push(
                    new Promise((resolve, reject) => {
                        updateSSH(authenticate, repo, dsc[0].id, sshkey, resolve)
                    })
                )
            }
        } else {
            //Create SSH
            promise_array.push(
                new Promise((resolve, reject) => {
                    createSSH(authenticate, repo, sshkey, resolve)
                })
            )
        }
    }
    //Wait all the promise to end

    let results = await Promise.all(promise_array);

    let responseEnd = {};
    responseEnd.error = [];
    if (results.length == 0) {
        responseEnd.status = "SSH_IDE";
        responseEnd.body = "Check if the keys are the same";
    } else {
        //Save the errors
        results.forEach(element => {
            if (element.error) {
                responseEnd.error.push(element.error)
            }
            responseEnd.status = (responseEnd.status == "FAILED") ? "FAILED" : element.status;
        });
    }
    resolve(responseEnd);
}

function waitSSH(authenticate, repo, sshkeys) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            postSSH(authenticate, repo, sshkeys, resolve).then(result => {
                resolve(result);
            });
        });
    });
}

function checkPOSTSSH(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkGETSSH(authenticate, repo).then(result => {
                if (result.status != "FAILED") {
                    waitSSH(authenticate, repo, result.body).then(result => {
                        resolve(result);
                    });
                } else {
                    resolve(result);
                }
            });
        });
    });
}

//GET

function checkGETSSH(authenticate, repo) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            let result = {};
            //Check if we have the username & token
            if (!authenticate || !authenticate.username || !authenticate.token) {
                result.status = "FAILED"
                result.error = [{ "Enviroment": "Enviroment Variables not set", "Remember": "Confi 'username' and 'token' variables for access bitbucket" }]
                resolve(result);
            }else if (!repo || !utils.checkInput(repo, inputsRequest.repo)) {
                //Check the Input of the JSON is correct
                result.status = "FAILED"
                result.error = [{ "Input Error": "Please check the inputKeys value to solve the problem", "inputKeys": inputsRequest.repo }]
                resolve(result);
            }else{
                result = getSSH(authenticate, repo);
                resolve(result);
            }
        });
    });
}

async function getSSH(authenticate, repo) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/repositories/' + repo.usernameRepo + '/' + repo.reponame.toLowerCase() + '/deploy-keys';

    let method = "GET";
    const response = await utils.apiCall({}, url, method);

    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "SSH_INFO"
        responseEnd.body = response.body.values;
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    return responseEnd;
}


// --------------------------
// Repo - init call

function SSH(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkPOSTSSH(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

function retriveSSH(authenticate, repo, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkGETSSH(authenticate, repo).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

exports.SSH = SSH;
exports.retriveSSH = retriveSSH;