/**********************************************************************
* Project           : Bitbucket
* Program name      : getUserDataFromEmail.js
* Author            : AGM
* Date created      : 20190614
* Purpose           : Get the username data passing the email
**********************************************************************/

var utils = require("./utils");
var inputsRequest = require("./input");


async function getUserData(authenticate, email) {
    let url = 'https://' + authenticate.username + ':' + authenticate.token + '@api.bitbucket.org/2.0/users/' + email;

    let method = "GET";
    const response = await utils.apiCall({}, url, method);
    
    let responseEnd = {}
    if (response.statusCode >= 200 && response.statusCode < 300) {
        responseEnd.status = "BRANCH_PERM_INFO"
        responseEnd.body = response.body;
    } else {
        responseEnd.status = "FAILED"
        responseEnd.error = response;
    }
    return responseEnd;
}

function checkAuth(authenticate, email) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            let result = {};
            //Check if we have the username & token
            if (!authenticate || !authenticate.username || !authenticate.token) {
                result.status = "FAILED"
                result.error = [{ "Enviroment": "Enviroment Variables not set", "Remember": "Confi 'username' and 'token' variables for access bitbucket" }]
                resolve(result);
            }if(!utils.validateEmail(email)){
                result.status = "FAILED"
                result.error = ["Check the email"]
                resolve(result);
            }else{
                result = getUserData(authenticate, email);
                resolve(result);
            }
        });
    });
}
// --------------------------
// Repo - init call

function retriveUserDataFromEmail(authenticate, email, callback) {
    return new Promise((resolve) => {
        process.nextTick(() => {
            checkAuth(authenticate, email).then(result => {
                if (callback) { callback(result); }
                resolve(result);
            });
        });
    });
}

exports.retriveUserDataFromEmail = retriveUserDataFromEmail;